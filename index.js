const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const Project = require('./src/Router/router');
//for better version for console log (show time)
const logger = require('./config/logger');
//for environment that can be used globally
dotenv.config();
mongoose.Promise = global.Promise;
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//path that we will use as main
app.use('/project', Project);

mongoose.connect(process.env.url, { useNewUrlParser: true }, (error) => {
  if (error) throw error;
  logger.info('Successfully connected');
});
app.listen(process.env.port);

logger.info('Name'.includes('am'));

logger.info(`listening to port : ${process.env.port}`);
