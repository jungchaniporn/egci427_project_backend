const winston = require('winston');
const _ = require('lodash');
const moment = require('moment');

const {
  combine, timestamp, printf, colorize,
} = winston.format;
const myFormat = printf((data) => {
  let message;
  if (data.message) {
    // eslint-disable-next-line prefer-destructuring
    message = data.message;
  } else {
    message = _.omit(data, 'config', 'level', 'request', 'timestamp');
  }
  return `${data.level} [${moment(data.timestamp).format('MMDD-HHmm')}] ${JSON.stringify(message)}`;
});

const logger = winston.createLogger({
  levels: winston.config.syslog.levels,
  transports: [
    new winston.transports.Console({
      format: combine(
        colorize(),
        timestamp(),
        myFormat,
      ),
    }),
  ],
});

module.exports = logger;
