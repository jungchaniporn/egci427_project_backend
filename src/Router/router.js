const router = require('express').Router();
const Controller = require('../Controller/controller');

router.post('/initdb', Controller.initDB);

module.exports = router;
